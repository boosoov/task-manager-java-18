package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.Domain;

public interface IDomainService {

    void load(final Domain domain);

    void export(final Domain domain);

}
