package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String name);

    User removeById(String id);

    User removeByLogin(String name);

    User removeByUser(User user);

    void load(Collection<User> users);

    void load(User... users);

    User merge(User user);

    void merge(Collection<User> users);

    void merge(User...users);

    void clearAll();

    List<User> getListUsers();

}
