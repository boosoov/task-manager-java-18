package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML LOAD]");
        final XmlMapper xmlMapper = new XmlMapper();
        final String xml = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML)));
        Domain domain = xmlMapper.readValue(xml, Domain.class);
        domainService.load(domain);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
