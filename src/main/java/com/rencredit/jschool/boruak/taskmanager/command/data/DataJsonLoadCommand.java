package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA JSON LOAD]");
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON)));
        Domain domain = objectMapper.readValue(json, Domain.class);
        domainService.load(domain);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
