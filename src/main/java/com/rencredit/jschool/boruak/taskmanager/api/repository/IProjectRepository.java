package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    Project remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    void load(Collection<Project> projects);

    void load(Project... projects);

    Project merge(Project project);

    void merge(Collection<Project> projects);

    void merge(Project...projects);

    void clearAll();

    List<Project> getListProjects();

}
