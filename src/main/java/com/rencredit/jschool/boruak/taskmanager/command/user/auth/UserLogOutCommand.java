package com.rencredit.jschool.boruak.taskmanager.command.user.auth;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout system.";
    }

    @Override
    public void execute() {
        authService.logOut();
        System.out.println("Have been logout");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
