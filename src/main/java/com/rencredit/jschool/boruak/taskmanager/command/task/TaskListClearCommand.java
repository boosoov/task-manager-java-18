package com.rencredit.jschool.boruak.taskmanager.command.task;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

public class TaskListClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = authService.getUserId();
        taskService.clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
