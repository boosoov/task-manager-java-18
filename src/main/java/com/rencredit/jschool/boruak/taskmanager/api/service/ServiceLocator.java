package com.rencredit.jschool.boruak.taskmanager.api.service;

public interface ServiceLocator {

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IDomainService getDomainService();

}
