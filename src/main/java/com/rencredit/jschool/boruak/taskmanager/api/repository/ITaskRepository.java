package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll(String userId);

    void add(String userId, Task task);

    Task remove(String userId, Task task);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneById(String userId, String id);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

    void load(Collection<Task> tasks);

    void load(Task... tasks);

    Task merge(Task task);

    void merge(Collection<Task> tasks);

    void merge(Task...tasks);

    void clearAll();

    List<Task> getListTasks();

}
