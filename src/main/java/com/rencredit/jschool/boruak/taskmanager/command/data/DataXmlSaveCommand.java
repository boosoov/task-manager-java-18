package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-save";
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML SAVE]");
        final Domain domain = new Domain();
        domainService.export(domain);

        final File file =  new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final XmlMapper xmlMapper = new XmlMapper();
        final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.FILE_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
