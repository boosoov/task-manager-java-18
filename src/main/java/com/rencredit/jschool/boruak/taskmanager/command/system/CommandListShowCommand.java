package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;

import java.util.Arrays;

public class CommandListShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

}
