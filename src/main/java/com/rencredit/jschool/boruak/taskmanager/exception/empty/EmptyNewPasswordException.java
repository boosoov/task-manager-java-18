package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyNewPasswordException extends RuntimeException {

    public EmptyNewPasswordException() {
        super("Error! New password is empty...");
    }

}
