package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class SystemExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-e";
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
