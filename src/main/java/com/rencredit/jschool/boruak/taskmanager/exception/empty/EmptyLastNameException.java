package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyLastNameException extends RuntimeException {

    public EmptyLastNameException() {
        super("Error! Last name is empty...");
    }

}
