package com.rencredit.jschool.boruak.taskmanager.exception.unknown;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException() {
        super("Error! Unknown command...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Unknown command ``" + command + "``...");
    }

}
