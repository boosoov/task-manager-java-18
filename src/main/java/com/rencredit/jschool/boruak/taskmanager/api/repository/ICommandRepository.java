package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.dto.Command;

import java.util.Map;

public interface ICommandRepository {

    Map<String, AbstractCommand> getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

    void registryCommand();

}
