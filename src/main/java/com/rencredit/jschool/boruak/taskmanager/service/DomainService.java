package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IDomainService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;

public class DomainService implements IDomainService {

    private final ServiceLocator serviceLocator;

    public DomainService(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
    }
    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(serviceLocator.getProjectService().getListProjects());
        domain.setTasks(serviceLocator.getTaskService().getListTasks());
        domain.setUsers(serviceLocator.getUserService().getListUsers());
    }

}
