package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataJsonClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-clear";
    }

    @Override
    public String description() {
        return "Remove json data.";
    }

    @Override
    public void execute() throws IOException {
        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
